// The MIT License (MIT)
//---------------------------------------
// Copyright (c) 2019 Paul Nicholls
// Code Snippets Cookbook - 6502 Edition
//---------------------------------------
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the â€œSoftwareâ€), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#importonce

// maximum number of events; 5 should be plenty!!
.const MAX_EVENTS        = 5

// tables for each part of the event "record"
event_enabled:        .fill MAX_EVENTS,0 // not triggered if not enabled
event_retrigger:      .fill MAX_EVENTS,0 // event will reset automatically
event_counter_lsb:    .fill MAX_EVENTS,0 // counter lsb/msb
event_counter_msb:    .fill MAX_EVENTS,0
event_maxCounter_lsb: .fill MAX_EVENTS,0 // reset counter lsb/msb values to use
event_maxCounter_msb: .fill MAX_EVENTS,0
event_address_lsb:    .fill MAX_EVENTS,0 // event call address lsb/msb when triggered
event_address_msb:    .fill MAX_EVENTS,0

// helper fake instruction to copy a byte of data from
// src to dst; 
//   src can be a value or an address, or an instruction operand (ie, $a000,x)
//   dst can be a an address, or an instruction operand (ie, $a000,x)
.pseudocommand copy8 src : dst {
  lda src
  sta dst
}

//-----------------------------------------------------
// input:  not used
// output: A & X reg = index of free event index (if not enabled), or 255 if not found
//-----------------------------------------------------
findFreeEvent: {
  ldx #0
loop:
  lda event_enabled,x
  beq foundEvent // this event_enabled = 0 so is free
  inx
  cpx #MAX_EVENTS
  bne loop // not reached MAX_EVENTS so loop back
  lda #255 // free record not found result
  rts
foundEvent:
  txa
  rts
}

// pushes an event onto the event queue if it can find
// a free slot
//
// seconds       = delay before triggering event.
// eventAddress  = address of routine to call when event is triggered
// reTriggerable = false (0) or true (1); if true, event retriggers every seconds.
// FPS           = number of times per second it will be triggered; 50 = PAL, 60 = NTSC
.macro PushEvent(seconds,eventAddress,reTriggerable,FPS) {
  .var counterValue = floor(seconds * FPS)

  jsr findFreeEvent
  bmi freeEventNotFound // branch to not found if A = 255 (= -1 in twos-compliment) 
  
  // must have found a free event so use this one to write data to.
  // copy all data to this event using x as the index
  :copy8 #1                   : event_enabled,x
  :copy8 #reTriggerable       : event_retrigger,x
  :copy8 #<counterValue       : event_counter_lsb,x
  :copy8 #>counterValue       : event_counter_msb,x
  :copy8 #<counterValue       : event_maxCounter_lsb,x
  :copy8 #>counterValue       : event_maxCounter_msb,x
  :copy8 #<[eventAddress - 1] : event_address_lsb,x
  :copy8 #>[eventAddress - 1] : event_address_msb,x
freeEventNotFound:
}

// disable all events
clearEvents: {
  ldx #0
loop:
  lda #0
  sta event_enabled,x
  inx
  cpx #MAX_EVENTS
  bne loop
  rts
}

// updates events that are active, and if counter = 0
// triggers the event address routine
updateEvents: {
  ldx #0
loop:
  lda event_enabled,x
  beq eventNotEnabled

  // event is enabled so check counter = 0
  jsr checkEventCounter
eventNotEnabled:
  inx
  cpx #MAX_EVENTS
  bne loop
  rts

checkEventCounter:
  lda event_counter_lsb,x
  bne !+           // lsb not zero so update counter
  lda event_counter_msb,x
  beq triggerEvent // msb = 0, so counter = $0000, and event is triggered
  // decrement event counter
  //msb is <> 0 so decrement that by 1
  dec event_counter_msb,x
!:
  // lsb is <> 0 so decrement that by 1
  dec event_counter_lsb,x
  rts

triggerEvent:
  // set event to not enabled
  lda #0
  sta event_enabled,x

  lda event_retrigger,x
  beq !+ // not retriggerable so skip next bit
  // is retriggable so reset counter with maxCounter
  :copy8 event_maxCounter_lsb,x : event_counter_lsb,x
  :copy8 event_maxCounter_msb,x : event_counter_msb,x

  // set event to enabled for next update phase
  lda #1
  sta event_enabled,x
!:
  // save x
  txa
  pha

  jsr callEventAddress
  
  //restore x
  pla
  tax  
  rts

callEventAddress:
  // trigger counter event
  lda event_address_msb,x
  pha
  lda event_address_lsb,x
  pha
  rts
}