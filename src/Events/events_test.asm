// The MIT License (MIT)
//---------------------------------------
// Copyright (c) 2019 Paul Nicholls
// Code Snippets Cookbook - 6502 Edition
//---------------------------------------
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the â€œSoftwareâ€), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

:BasicUpstart2(mainStartup)

* = $080d "main code"

.import source "events_rtl.asm"

mainStartup: {
  // setup events
  // this event: trigger every 1 second, event routine address,retrigger when finished,updates per second
  :PushEvent(1,tickEvent,1,50)
  
  // this event: trigger every 0.5 second, event routine address,retrigger when finished,updates per second
  :PushEvent(0.5,tickEvent2,1,50)

  // init tickEvent digit
  lda #48
  sta 1024

  // setup interrupt
  sei            // disable interrupts
  lda #<IRQaddr  // Install RASTER IRQ
  ldx #>IRQaddr  // into Hardware
  sta $0314      // Interrupt Vector
  stx $0315      
  cli            // enable interrupts

  // back to basic
  rts
}

IRQaddr: {
  // this interrupt is called once per interrupt clock,
  // ie. 50 times a second in the case of a PAL machine.
  jsr updateEvents

  // back to standard interrupt routine
  jmp $ea31 
}

tickEvent: {
  // inc digit on screen
  // and reset if gone past '9'
  inc 1024
  lda 1024
  cmp #48+10
  bne !+
  lda #48
  sta 1024
!:
  rts
}

tickEvent2: {
  // increment the color of the border
  inc $d020
  rts
}