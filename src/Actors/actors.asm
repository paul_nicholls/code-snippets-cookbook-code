// The MIT License (MIT)
//---------------------------------------
// Copyright (c) 2019 Paul Nicholls
// Code Snippets Cookbook - 6502 Edition
//---------------------------------------
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the â€œSoftwareâ€), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

:BasicUpstart2(start)

* = $080d "main code"

.const BALLOON_FRAME = 13

// assume default screen address
.const SCREEN_ADDRESS = 1024

.import source "actors_rtl.asm"
.import source "..\include\c64_vic.asm"

balloonData:
	.byte 0,127,0,1,255,192,3,255,224,3,231,224
	.byte 7,217,240,7,223,240,2,217,240,3,231,224
	.byte 3,255,224,3,255,224,2,255,160,1,127,64
	.byte 1,62,64,0,156,128,0,156,128,0,73,0,0,73,0,0
	.byte 62,0,0,62,0,0,62,0,0,28,0

start: {
  // copy actor frame data to sprite block "BALLOON_FRAME"
  jsr copySpriteData

  :SPAWN_ACTOR(100,100,white,BALLOON_FRAME)
  :SPAWN_ACTOR(50,50+30,red,BALLOON_FRAME)
  :SPAWN_ACTOR(300,70,cyan,BALLOON_FRAME)

  // disable interrupts
  sei
!:
  // wait for vertical blank
  vic_waitForRaster(251)

  jsr vic_copyActorsToHardware
  jmp !-
}

copySpriteData: {
  ldx #62
!:
  lda balloonData,x
  sta BALLOON_FRAME*64,x
  dex
  bne !-

  rts
}
