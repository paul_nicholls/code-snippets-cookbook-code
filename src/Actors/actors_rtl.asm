// The MIT License (MIT)
//---------------------------------------
// Copyright (c) 2019 Paul Nicholls
// Code Snippets Cookbook - 6502 Edition
//---------------------------------------
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the â€œSoftwareâ€), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

.const MAX_ACTORS     = 8
actor_enabled:     .fill MAX_ACTORS,0

// actor x pos information (lsb/msb)
actor_xlsb:        .fill MAX_ACTORS,0
actor_xmsb:        .fill MAX_ACTORS,0

// actor y pos information (lsb)
actor_ylsb:        .fill MAX_ACTORS,0

// actor color information
actor_multicolor:  .fill MAX_ACTORS,0
actor_color:       .fill MAX_ACTORS,0

// actor frame (sprite pointer) information
actor_frame:       .fill MAX_ACTORS,0

//-------------------------------------------------------------------------
// getFreeActorIndex
//-------------------------------------------------------------------------
// returns 255 in acc if not found, 
// otherwise returns x index of free actor
//-------------------------------------------------------------------------
getFreeActorIndex: {
  ldx #0 // start at first actor
!:
  lda actor_enabled,x
  beq foundActor // is zero so not used
  inx
  cpx #MAX_ACTORS
  bne !-
  lda #255
  rts
foundActor:
  txa
  rts
}

.macro SPAWN_ACTOR(xPos,yPos,color,frame) {
  lda #<xPos
  sta spawnActor.xPos + 0

  lda #>xPos
  sta spawnActor.xPos + 1

  lda #yPos
  sta spawnActor.yPos

  lda #color
  sta spawnActor.color

  lda #frame
  sta spawnActor.frame

  jsr spawnActor
}

spawnActor: {
  jsr getFreeActorIndex
  bmi exitSpawnActor // not found as A = 255 (-1)

  // enable actor
  lda #1
  sta actor_enabled,x

  // set actor x pos (lsb/msb)
  lda xPos + 0
  sta actor_xlsb,x

  lda xPos + 1
  sta actor_xmsb,x

  // set actor y pos (lsb only)
  lda yPos
  sta actor_ylsb,x

  // disable actor multicolor
  lda #0
  sta actor_multicolor,x

  // set actor color
  lda color
  sta actor_color,x

  // set actor frame (sprite pointer)
  lda frame
  sta actor_frame,x

exitSpawnActor:
  rts

// local variables for spawnActor
xPos:  .byte 0,0
yPos:  .byte 0
color: .byte 0
frame: .byte 0
}

vic_copyActorsToHardware: {
  // do sprites 7 downto 0
  ldx #$07 // actor index
  ldy #$0e // sprite register index
      
loop: 
  //write actor y into sprite y register ($d001, $d003, etc...)
  lda actor_ylsb,x
  sta $d001,y                 

  //write actor x lsb into sprite x register ($d000, $d002, etc...)
  lda actor_xlsb,x
  sta $d000,y                 

  lda actor_xmsb,x
  ror                         // rotate actor x msb into carry flag
  rol $d010                   // rotate carry flag into $d010 (sprite x msb bit enabled), repeat 8 times and all bits are set

  lda actor_enabled,x
  ror                         // rotate actor enabled lsb into carry flag
  rol $d015                   // rotate carry flag into $d015 (sprite enabled), repeat 8 times and all bits are set

  lda actor_multicolor,x
  ror                         // rotate actor muticolor lsb into carry flag
  rol $d01c                   // rotate carry flag into $d01c (sprite multicolor enabled), repeat 8 times and all bits are set

  // write actor color into sprite color register
  lda actor_color,x
  sta $d027,x

  // sprite pointers; offset from current screen position by 1016
  // write actor sprite pointer (frame) into sprite pointer register
  lda actor_frame,x
  sta SCREEN_ADDRESS + 1016,x
  
  // decrement y twice as sprite x/y are in staggered pairs
  dey
  dey
  dex
  // not wrapped around to 255 yet?, jump back to loop
  bpl loop
  rts
}
