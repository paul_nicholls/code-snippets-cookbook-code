// The MIT License (MIT)
//---------------------------------------
// Copyright (c) 2019 Paul Nicholls
// Code Snippets Cookbook - 6502 Edition
//---------------------------------------
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the â€œSoftwareâ€), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#importonce

  .var vic_spr0_x      = $D000
  .var vic_spr0_y      = $D001
  .var vic_spr1_x      = $D002
  .var vic_spr1_y      = $D003
  .var vic_spr2_x      = $D004
  .var vic_spr2_y      = $D005
  .var vic_spr3_x      = $D006
  .var vic_spr3_y      = $D007
  .var vic_spr4_x      = $D008
  .var vic_spr4_y      = $D009
  .var vic_spr5_x      = $D00A
  .var vic_spr5_y      = $D00B
  .var vic_spr6_x      = $D00C
  .var vic_spr6_y      = $D00D
  .var vic_spr7_x      = $D00E
  .var vic_spr7_y      = $D00F
  .var vic_spr_hi_x    = $D010
  .var vic_cr1         = $D011
  .var vic_raster      = $D012
  .var vic_lp_x        = $D013
  .var vic_lp_y        = $D014
  .var vic_spr_ena     = $D015
  .var vic_cr2         = $D016
  .var vic_spr_exp_y   = $D017
  .var vic_mem         = $D018
  .var vic_irq         = $D019
  .var vic_irq_ena     = $D01A
  .var vic_spr_dp      = $D01B
  .var vic_spr_mcolor  = $D01C
  .var vic_spr_exp_x   = $D01D
  .var vic_spr_ss_col  = $D01E
  .var vic_spr_sd_col  = $D01F
  .var vic_border      = $D020
  .var vic_bg_color0   = $D021
  .var vic_bg_color1   = $D022
  .var vic_bg_color2   = $D023
  .var vic_bg_color3   = $D024
  .var vic_chr_mcolor1 = $D022
  .var vic_chr_mcolor2 = $D023
  .var vic_spr_color1  = $D025
  .var vic_spr_color2  = $D026
  .var vic_spr0_color  = $D027
  .var vic_spr1_color  = $D028
  .var vic_spr2_color  = $D029
  .var vic_spr3_color  = $D02A
  .var vic_spr4_color  = $D02B
  .var vic_spr5_color  = $D02C
  .var vic_spr6_color  = $D02D
  .var vic_spr7_color  = $D02E
  .var vic_spr_coord   = $D000
  .var vic_spr_color   = $D027

  .var vic_color_ram   = $d800
 
  .const black         = 0
  .const white         = 1
  .const red           = 2
  .const cyan          = 3
  .const purple        = 4
  .const green         = 5
  .const blue          = 6
  .const yellow        = 7
  .const orange        = 8
  .const brown         = 9
  .const light_red     = 10
  .const dark_grey     = 11
  .const dark_gray     = 11
  .const medium_grey   = 12
  .const medium_gray   = 12
  .const light_green   = 13
  .const light_blue    = 14
  .const light_grey    = 15
  .const light_gray    = 15

.macro vic_waitForRaster(raster) {
!:
  lda vic_raster
  cmp #raster
  bne !-
}