// The MIT License (MIT)
//---------------------------------------
// Copyright (c) 2019 Paul Nicholls
// Code Snippets Cookbook - 6502 Edition
//---------------------------------------
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

:BasicUpstart2(start)

* = $080d "main code"

start:
  jmp mainStartup

commandIndex: .byte 0

mainStartup: {
  lda commandIndex // load index of routine to jump to
  jsr execCommand
  rts

execCommand:
  // transfer index in A -> X ready for table lookup
  tax
  lda cmdJumpTable_hi,x
  pha
  lda cmdJumpTable_lo,x
  pha
  rts // jump to location in stack

// can have up to 256 different routines!!
// addresses must be - 1 from the destination "jump"
// due to using RTS
cmdJumpTable_lo: .byte <displayInventoryCmd-1,<loadGameCmd-1,<saveGameCmd-1
cmdJumpTable_hi: .byte >displayInventoryCmd-1,>loadGameCmd-1,>saveGameCmd-1

.const SCREEN_ADDRESS = 1024
.const SCREEN_WIDTH   = 40

.const addressPntr    = $fb // Indirect Indexed zero-page pointer

.const CR             = 255
.const END            = 0

// inventory
inventoryData: 
  .text "inventory"
  .byte CR
  .text "---------"
  .byte CR
  .text "01 x apple"
  .byte CR
  .text "10 x grapes"
  .byte CR
  .text "01 x cheese wheel"
  .byte CR
  .text "01 x leather armor"
  .byte END

setupScreenPointer: {
  // setup screen pointer
  // store lo/hi byte of screen address
  // (top left character)
  // in addressPntr + 0/ + 1 respectively
  lda #<SCREEN_ADDRESS
  sta addressPntr + 0
  lda #>SCREEN_ADDRESS
  sta addressPntr + 1
  rts
}

clearScreen: {
  // clear top part of screen (first 256 characters)
  ldy #0
  lda #32 // space character
!:
  sta (addressPntr),y
  dey
  bne !-

  rts
}

displayInventory: {
  jsr setupScreenPointer
  jsr clearScreen

  ldx #0 // inventory data index
  ldy #0 // screen x offset on current line
loop:
  // load data from inventory
  lda inventoryData,x

  // is this data equal to 0, ie. end of data?
  beq finished // yes, so branch to finished

  // is this data equal to newline character?
  cmp #CR
  beq newLine // yes so branch to newline

  // is ordinary data so write to the screen
  // at addressPntr + y
  sta (addressPntr),y
  
  // inc x ready for next data read
  inx
  // inc the current screen x offset on current line
  iny
  // read next data
  jmp loop
  // new line
newLine:
  // add 40 to the screen pointer
  clc
  lda addressPntr + 0
  adc #<SCREEN_WIDTH
  sta addressPntr + 0
  lda addressPntr + 1
  adc #>SCREEN_WIDTH
  sta addressPntr + 1

  // is new line so zero screen x location offset (in Y reg)
  ldy #0
  
  // inc x ready for next data read
  inx
  // read next data
  jmp loop
finished:
  rts
}

displayInventoryCmd:
  jsr displayInventory
  rts // return back to the next instruction after "jsr execCommand"

loadGameCmd:
  rts // return back to the next instruction after "jsr execCommand"

saveGameCmd:
  rts // return back to the next instruction after "jsr execCommand"
rts
}