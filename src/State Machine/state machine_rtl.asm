// The MIT License (MIT)
//---------------------------------------
// Copyright (c) 2019 Paul Nicholls
// Code Snippets Cookbook - 6502 Edition
//---------------------------------------
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#importonce

// state indices
.const STATE_NOSTATE    = -1
.const STATE_TILESCREEN = 0
.const STATE_PLAYING    = 1
.const STATE_DYING      = 2
.const STATE_GAMEOVER   = 3

gameState: {
  // current state index
  currentState: .byte STATE_NOSTATE
  
  // state tables
  onEnterState_lo:
    .byte <stateTitlescreen.onEnter-1,<statePlaying.onEnter-1,<stateDying.onEnter-1,<stateGameOver.onEnter-1
  onEnterState_hi:
    .byte >stateTitlescreen.onEnter-1,>statePlaying.onEnter-1,>stateDying.onEnter-1,>stateGameOver.onEnter-1
  
  onUpdateState_lo:
    .byte <stateTitlescreen.onUpdate-1,<statePlaying.onUpdate-1,<stateDying.onUpdate-1,<stateGameOver.onUpdate-1
  onUpdateState_hi:
    .byte >stateTitlescreen.onUpdate-1,>statePlaying.onUpdate-1,>stateDying.onUpdate-1,>stateGameOver.onUpdate-1
  
  onExitState_lo:
    .byte <stateTitlescreen.onExit-1,<statePlaying.onExit-1,<stateDying.onExit-1,<stateGameOver.onExit-1
  onExitState_hi:
    .byte >stateTitlescreen.onExit-1,>statePlaying.onExit-1,>stateDying.onExit-1,>stateGameOver.onExit-1
  
  // call current state's onEnter routine
  callOnEnterState: {
    ldx currentState
    cpx #STATE_NOSTATE
    bne !+
    rts
  !:
    lda onEnterState_hi,x
    pha
    lda onEnterState_lo,x
    pha
    rts
  }
  
  // call current state's onExit routine
  callOnExitState: {
    ldx currentState
    cpx #STATE_NOSTATE
    bne !+
    rts
  !:
    lda onExitState_hi,x
    pha
    lda onExitState_lo,x
    pha
    rts
  }
  
  // call current state's onUpdate routine
  callOnUpdateState: {
    ldx currentState
    cpx #STATE_NOSTATE
    bne !+
    rts
  !:
    lda onUpdateState_hi,x
    pha
    lda onUpdateState_lo,x
    pha
    rts
  }
  
  // enter new state (if applicable)
  // A reg = new state index
  changeState: {
    cmp currentState
    bne isDifferentState
    // early exit as no change
    rts
  
  isDifferentState:
    // save new state index
    pha

    lda currentState
    cmp #STATE_NOSTATE
    beq isNoState  

    // call onExit routine of existing state (if applicable)
    jsr callOnExitState

  isNoState:
    // restore new state index
    pla
    sta currentState
  
    // call onEnter routine of new state
    jsr callOnEnterState  
    rts
  }
}

// states and their routines
// ---------------------------
// STATE_TILESCREEN
// ---------------------------
stateTitlescreen: {
  onEnter: {
    rts
  }

  onUpdate: {
    rts
  }

  onExit: {
    rts
  }
}

// ---------------------------
// STATE_PLAYING
// ---------------------------
statePlaying: {
  onEnter: {
    rts
  }

  onUpdate: {
    rts
  }

  onExit: {
    rts
  }
}

// ---------------------------
// STATE_DYING
// ---------------------------
stateDying: {
  onEnter: {
    rts
  }

  onUpdate: {
    rts
  }

  onExit: {
    rts
  }
}

// ---------------------------
// STATE_GAMEOVER
// ---------------------------
stateGameOver: {
  onEnter: {
    rts
  }

  onUpdate: {
    rts
  }

  onExit: {
    rts
  }
}

// helper macros
.macro ChangeState(state) {
  lda #state
  jsr gameState.changeState 
}

.macro UpdateState() {
  jsr gameState.callOnUpdateState
}