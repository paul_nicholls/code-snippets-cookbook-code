#importonce

.const OSWRCH = $FFEE

vdu_cursorOff: {
  _main_:
  lda #23; jsr OSWRCH
  lda #1; jsr OSWRCH
  lda #0; jsr OSWRCH                                
  lda #0; jsr OSWRCH
  lda #0; jsr OSWRCH
  lda #0; jsr OSWRCH
  lda #0; jsr OSWRCH
  lda #0; jsr OSWRCH
  lda #0; jsr OSWRCH
  lda #0; jsr OSWRCH
  rts
}

.macro vdu_gcol(mode,color) {
// Define graphics colour (vdu 18) 
lda #18;    jsr OSWRCH
lda #mode;  jsr OSWRCH
lda #color; jsr OSWRCH
}

.macro vdu_mode(value) {
// sets screen mode
  lda #22
  jsr OSWRCH
  lda #value
  jsr OSWRCH
}

.macro vdu_plot(mode,x,y) {
// source immediate values
// plots point in x,y using mode 
  lda #25 //vdu 25
  jsr OSWRCH
  lda #mode; jsr OSWRCH
  lda #<x; jsr OSWRCH
  lda #>x; jsr OSWRCH
  lda #<y; jsr OSWRCH
  lda #>y; jsr OSWRCH
}

.macro vdu_plot_vv(mode,x,y) {
// v = variable source (word)
// plots point in x,y using mode 
  lda #25 //vdu 25
  jsr OSWRCH
  lda #mode; jsr OSWRCH
  lda x + 0; jsr OSWRCH
  lda x + 1; jsr OSWRCH
  lda y + 0; jsr OSWRCH
  lda y + 1; jsr OSWRCH
}

.macro vdu_move(x,y) {
// mode = 4 (move to location)
  :vdu_plot(4,x,y)
}

.macro vdu_move_vv(x,y) {
// v = variable source (word)
// mode = 4 (move to location)
  :vdu_plot_vv(4,x,y)
}

.macro vdu_draw(x,y) {
// draw from last move to location x,y
  :vdu_plot(5,x,y)
}

.macro vdu_draw_vv(x,y) {
// v = variable source (word)
// draw from last move to location x,y
  :vdu_plot_vv(5,x,y)
}

.macro vdu_19(logical,physical) {
// Define logical colour
// https://beebwiki.mdfs.net/VDU_19
  lda #19 //vdu 19
  jsr OSWRCH
  lda #logical; jsr OSWRCH
  lda #physical; jsr OSWRCH
  lda #0; jsr OSWRCH; jsr OSWRCH; jsr OSWRCH
}