* = $1D00 "main code"

jmp bbc_test

.import source "vdu\bbc_vdu.asm"

.macro saveRegisters() {
  php; pha; txa; pha; tya; pha
}

.macro restoreRegisters() {
  pla; tay; pla; tax; pla; plp
}

bbc_test: {
  :saveRegisters()
// set to mode 4
  :vdu_mode(4)

// set graphics drawing mode to XOR; good for drawing and then erasing things easily
  :vdu_gcol(3,3)

// set the background colour to red and the draw colour to yellow
  :vdu_19(1,3)
  :vdu_19(0,1)

// draw a line from (50,50) to (300,300)
  :vdu_move(50,50)
  :vdu_draw(300,300)

  :restoreRegisters()
  
  jmp *
}