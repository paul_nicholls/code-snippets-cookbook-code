md myfiles
@echo off
set srcprogname="bbc_example.asm"
set kickassemfile="C:\utilities\KickAssembler\KickAss.jar"
set progname=BBCTEST
set progaddr=1D00

@echo on
del myfiles\%progname%
del %progname%.ssd

java.exe -jar %kickassemfile% %srcprogname% -o "myfiles\%progname%" -binfile

(
echo *BASIC
echo *RUN %progname%
)> myfiles\!BOOT

(
echo %progname% FFFF%progaddr% FFFF%progaddr%
)> myfiles\%progname%.inf

(
echo !BOOT
echo %progname%
)> myfiles\filelist.txt

"..\utilities\mkimg.exe" -fs DFS -size 200K %progname%.ssd myfiles -opt 3 -i@myfiles\filelist.txt -title "%progname%" -pad
pause